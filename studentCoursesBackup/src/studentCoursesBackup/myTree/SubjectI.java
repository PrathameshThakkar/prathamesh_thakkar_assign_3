/**
 * 
 */
package studentCoursesBackup.myTree;

import java.util.ArrayList;

/**
 * @author prathamesh
 *
 */
public interface SubjectI {
	void registerObserver(ObserverI o);
	void removeObserver(ObserverI o);
	void notifyObserver(ArrayList<Node> arr, String type, String value);
}
