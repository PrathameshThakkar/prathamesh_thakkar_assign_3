/**
 * 
 */
package studentCoursesBackup.myTree;

/**
 * @author prathamesh
 *
 */
public interface FilterI {
	boolean check(String filterString);
}
