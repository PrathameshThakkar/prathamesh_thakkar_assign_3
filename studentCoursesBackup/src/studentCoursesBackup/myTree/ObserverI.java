/**
 * 
 */
package studentCoursesBackup.myTree;

/**
 * @author prathamesh
 *
 */
public interface ObserverI {
	void update(Node o, String type, String value );
}
