/**
 * 
 */
package studentCoursesBackup.driver;


import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.FileDisplayInterface;
import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.Results;
import studentCoursesBackup.util.TreeBuilder;

/**
 * @author prathamesh
 *
 */
public class Driver {
	public static void main(String[] args){
		String[] separate = null;
		String inputFile = null;
		String outputFile = null;
		String outputFile1 = null;
		String deleteFile = null;
		String outputFile2 = null;
		if(args.length != 5){
			System.err.println("Expected five arguments");
			System.exit(1);
		}
		else{
			inputFile = args[0];
			deleteFile = args[1];
			outputFile = args[2];
			outputFile1 = args[3];
			outputFile2 = args[4];
		
		}
		
		FileProcessor fp = new FileProcessor(inputFile);
		String line = null;
		FileDisplayInterface res_orig = new Results();
		FileDisplayInterface res_backup_1 = new Results();
		FileDisplayInterface res_backup_2 = new Results();
		TreeBuilder bst_orig = new TreeBuilder(res_orig);
		TreeBuilder bst_backup_1 = new TreeBuilder(res_backup_1);
		TreeBuilder bst_backup_2 = new TreeBuilder(res_backup_2);
		while((line = fp.readLine()) != null){
			//System.out.println(line + "\n");
			line.split(":");
			separate = line.split(":");
			String course = separate[1];
			String number = separate[0];
			if ((course.length() == 1) && course.matches("[A-K?]"))
			{
				int bNumber = Integer.parseInt(number);
				Node check = bst_orig.find(bNumber);
				Node orig_node = null;
				Node backup_1 = null;
				Node backup_2 = null;
				if (check == null){
					orig_node = new Node(bNumber, course);
					bst_orig.insert(orig_node);
					if(orig_node instanceof Node)
					{
						backup_1 = (Node) orig_node.Clone();
						bst_backup_1.insert(backup_1);
						backup_2 = (Node) orig_node.Clone();
						bst_backup_2.insert(backup_2);
						orig_node.registerObserver(backup_1);
						orig_node.registerObserver(backup_2);
					}
				}else if(!(check.courses.contains(course))){
					check.courses.add(course);
					check.notifyObserver(check.getObservers(), "insert", course);
				}
			}
		}

		FileProcessor fp1 = new FileProcessor(deleteFile);
		String line1 = null;
		String[] seperate1 = null;
		while((line1 = fp1.readLine()) != null){
			seperate1 = line1.split(":");
			String deleteCourse = seperate1[1];
			if ((deleteCourse.length() == 1) && deleteCourse.matches("[A-K?]")){
				String deleteNumber = seperate1[0];
				int deleteBNumber = Integer.parseInt(deleteNumber);
				Node check1 = bst_orig.find(deleteBNumber);
				if(check1 != null){
					check1.courses.remove(deleteCourse);
					check1.notifyObserver(check1.getObservers(), "delete", deleteCourse);
				}
			}
		}
		bst_orig.display(bst_orig.root);
		res_orig.writeToFile(outputFile);
		bst_backup_1.display(bst_backup_1.root);
		res_backup_1.writeToFile(outputFile1);
		bst_backup_2.display(bst_backup_1.root);
		res_backup_2.writeToFile(outputFile2);
	}
}