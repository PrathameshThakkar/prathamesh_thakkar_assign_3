/**
 * 
 */
package studentCoursesBackup.util;
import studentCoursesBackup.myTree.Node;

/**
 * @author prathamesh
 *
 */

/* Citation
 * http://algorithms.tutorialhorizon.com/binary-search-tree-complete-implementation/
 */

public class TreeBuilder {
	public  Node root;
	private FileDisplayInterface results;
	public TreeBuilder(FileDisplayInterface resultsIn){
		this.root = null;
		results = resultsIn;
	}
	
	/**
	 * @param value, b-number to be searched
	 * @return Node, in which b-number is found 
	 */
	public Node find(int value){
		Node current = root;
		while(current != null){
			if(current.bNumber == value){
				return current;
			}else if(current.bNumber > value){
				current = current.left;
			}else{
				current = current.right;
			}
		}
		current = null;
		return current;
	}

	/**
	 * @param value, to find node to be used for deletion 
	 * @param letter, removing course corresponding to value  
	 * @return true if operation succeeded
	 */
	public boolean delete(int value, String letter){
		Node current = root;
		while(current != null){
			if(current.bNumber == value && current.courses.remove(letter) ){
				return true;
			}else if(current.bNumber > value){
				current = current.left;
			}else{
				current = current.right;
			}
		}
		return false;
	}

	/**
	 * @param node, Node to be inserted 
	 */
	public void insert(Node node){
		if(root == null){
			root = node;
			return;
		}
		Node current = root;
		Node parent = null;
		while(true){
			parent = current;
			if (node.bNumber < current.bNumber){
				current = current.left;
				if(current == null){
					parent.left = node;
					return;
				}
			}else{
				current = current.right;
				if(current == null){
					parent.right = node;
					return;
				}
			}
		}

	} 
	/**
	 * @param root, root of the tree to be printed in file
	 */
	public void display(Node root){
		boolean flag = false;
		if(root!=null){
			String finalm = "";
			String val = "";
			String print = "";
			display(root.left);
			val = "" + root.bNumber +":";
			for(String temp : root.courses){
				if(!flag){
					finalm = temp;
					flag = true;
				}else{
					print = "," + temp;
					finalm += print;
				}
			}
			((Results)results).storeNewResult(val + finalm) ;
			display(root.right);
		}
	}
}